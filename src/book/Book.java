package book;
@SuppressWarnings("unused")
public class Book {
    private String surnameOfAuthor;
    private String nameOfBook;
    private int yearOfPublication;

    Book(String surnameOfAuthor, String nameOfBook, int yearOfPublication) {
        this.surnameOfAuthor = surnameOfAuthor;
        this.nameOfBook = nameOfBook;
        this.yearOfPublication = yearOfPublication;
    }

    Book() {
        this("неизвестно", "неизвестно", 0);
    }

    public int getYearOfPublication() {
        return yearOfPublication;
    }

    public String getSurnameOfAuthor() {
        return surnameOfAuthor;
    }

    public String getNameOfBook() {
        return nameOfBook;
    }


    boolean isSameYears(Book book) {
        return book.getYearOfPublication() == this.yearOfPublication;
    }

    @Override
    public String toString() {
        return "Книга: {" +
                "Фамилия автора - '" + surnameOfAuthor + '\'' +
                ", Название книги -'" + nameOfBook + '\'' +
                ", Год публикации - " + yearOfPublication +
                '}';
    }
}

