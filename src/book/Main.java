package book;
public class Main {


    public static void main(String[] args) {

        Book book1 = new Book("Петров", "Познавший кровь", 2008);
        Book book2 = new Book("Толстой", "Война и мир", 1863);


        oneYearOfBook(book1.isSameYears(book2));
        System.out.println(book1.toString());
        System.out.println(book2.toString());
    }


    private static void oneYearOfBook(boolean sameYears) {
        if (sameYears) {
            System.out.println("Книги были выпущены в одном и том же году ");
        }
        if (!sameYears) {
            System.out.println("Книги не были выпущены в одном и том же году ");
        }
    }
}

